<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170317074309 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE music_library_artist (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(64) NOT NULL, country VARCHAR(8) NOT NULL, genre VARCHAR(32) NOT NULL, image VARCHAR(256) DEFAULT NULL, url VARCHAR(256) DEFAULT NULL, created_at DATETIME NOT NULL, modified_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE music_library_album (id INT AUTO_INCREMENT NOT NULL, artist_id INT DEFAULT NULL, title VARCHAR(64) NOT NULL, year INT NOT NULL, song_count INT NOT NULL, image VARCHAR(256) DEFAULT NULL, created_at DATETIME NOT NULL, modified_at DATETIME DEFAULT NULL, INDEX IDX_5FBC8A72B7970CF8 (artist_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE music_library_album ADD CONSTRAINT FK_5FBC8A72B7970CF8 FOREIGN KEY (artist_id) REFERENCES music_library_artist (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE music_library_album DROP FOREIGN KEY FK_5FBC8A72B7970CF8');
        $this->addSql('DROP TABLE music_library_artist');
        $this->addSql('DROP TABLE music_library_album');
    }
}

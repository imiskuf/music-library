<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170317074310 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

$sql = <<<SQL
INSERT INTO `music_library_artist` (`id`, `name`, `country`, `genre`, `image`, `url`, `created_at`, `modified_at`) VALUES
(2, 'Elán', 'SK', 'pop', 'elan.jpg', 'http://www.elan.cz/', '2017-03-17 08:59:59', '2017-03-17 08:59:59'),
(3, 'Michael Jackson', 'US', 'pop', 'Michael_Jackson_in_1988.jpg', NULL, '2017-03-17 09:03:44', '2017-03-17 09:03:44'),
(4, 'Blink 182', 'US', 'rock', 'Blink-182.jpg', 'http://www.blink182.com/', '2017-03-17 09:05:55', '2017-03-17 09:05:55')
SQL;

        $this->addSql($sql);

$sql = <<<SQL
INSERT INTO `music_library_album` (`id`, `artist_id`, `title`, `year`, `song_count`, `image`, `created_at`, `modified_at`) VALUES
(1, 2, 'Živých ná nedostanú', 2014, 13, 'elan (1).jpg', '2017-03-17 09:00:38', '2017-03-17 09:00:38'),
(2, 2, 'Rabaka', 1989, 12, 'rabaka.jpg', '2017-03-17 09:01:36', '2017-03-17 09:01:36'),
(3, 2, 'Elán 3000', 2002, 15, 'elan-3000-kopie.jpg', '2017-03-17 09:02:38', '2017-03-17 09:02:38'),
(4, 3, 'Thriller', 1982, 20, 'Michael_Jackson_-_Thriller.png', '2017-03-17 09:05:17', '2017-03-17 09:05:17'),
(5, 4, 'Take off your pants and jacket', 2003, 16, 'Blink-182_-_Take_Off_Your_Pants_and_Jacket_cover.jpg', '2017-03-17 09:06:24', '2017-03-17 09:06:24');
SQL;

        $this->addSql($sql);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM music_library_album');
        $this->addSql('DELETE FROM music_library_artist');
    }
}

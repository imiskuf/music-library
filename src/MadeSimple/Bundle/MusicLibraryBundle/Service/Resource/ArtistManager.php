<?php

namespace MadeSimple\Bundle\MusicLibraryBundle\Service\Resource;

use AppBundle\Service\AbstractORMResourceManager;
use MadeSimple\Bundle\MusicLibraryBundle\Entity\Artist;

/**
 * Class ArtistManager
 */
class ArtistManager extends AbstractORMResourceManager
{
    /**
     * Returns resource class
     * @return string
     */
    protected function getResourceClass()
    {
        return Artist::class;
    }
}

<?php

namespace MadeSimple\Bundle\MusicLibraryBundle\Service\Resource;

use AppBundle\Service\AbstractORMResourceManager;
use MadeSimple\Bundle\MusicLibraryBundle\Entity\Album;

/**
 * Class ArtistManager
 */
class AlbumManager extends AbstractORMResourceManager
{
    /**
     * Returns resource class
     * @return string
     */
    protected function getResourceClass()
    {
        return Album::class;
    }
}

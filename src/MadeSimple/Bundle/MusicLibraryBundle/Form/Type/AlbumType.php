<?php

namespace MadeSimple\Bundle\MusicLibraryBundle\Form\Type;

use MadeSimple\Bundle\MusicLibraryBundle\Entity\Album;
use MadeSimple\Bundle\MusicLibraryBundle\Entity\Artist;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

/**
 * Class ArtistType
 * @package MadeSimple\Bundle\MusicLibraryBundle\Form\Type
 */
class AlbumType extends AbstractType
{
    const FORM_TYPE_NAME = 'album';

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('artist', EntityType::class, [
                'class' => Artist::class,
                'choice_label' => 'name',
                'required' => true
            ])
            ->add('title', TextType::class)
            ->add('imageFile', VichImageType::class, [
                'label' => 'Image',
                'required' => false,
            ])
            ->add('year', TextType::class, [
                'label' => 'Year of publication'
            ])
            ->add('songCount', TextType::class, [
                'label' => 'Number of songs'
            ])
            ->add('submit', SubmitType::class);
    }

    /**
     * @param OptionsResolver $resolver
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => true,
            'csrf_token_id'   => 'artist.token',
            'data_class'      => Album::class
        ]);
    }

    /**
     * @return null|string
     */
    public function getBlockPrefix()
    {
        return self::FORM_TYPE_NAME;
    }
}

<?php


namespace MadeSimple\Bundle\MusicLibraryBundle\Form\Type;

use MadeSimple\Bundle\MusicLibraryBundle\Entity\Artist;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

/**
 * Class ArtistType
 * @package MadeSimple\Bundle\MusicLibraryBundle\Form\Type
 */
class ArtistType extends AbstractType
{
    const FORM_TYPE_NAME = 'artist';

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('imageFile', VichImageType::class, [
                'label' => 'Image',
                'required' => false,
            ])
            ->add('country', CountryType::class)
            ->add('genre', TextType::class)
            ->add('url', TextType::class, [
                'required' => false
            ])
            ->add('submit', SubmitType::class);
    }

    /**
     * @param OptionsResolver $resolver
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => true,
            'csrf_token_id'   => 'artist.token',
            'data_class'      => Artist::class
        ]);
    }

    /**
     * @return null|string
     */
    public function getBlockPrefix()
    {
        return self::FORM_TYPE_NAME;
    }
}

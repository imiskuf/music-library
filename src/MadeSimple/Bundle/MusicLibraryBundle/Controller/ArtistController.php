<?php

namespace MadeSimple\Bundle\MusicLibraryBundle\Controller;

use AppBundle\Model\Exception\ResourceNotFoundException;
use AppBundle\Model\Exception\ResourceOperationException;
use MadeSimple\Bundle\MusicLibraryBundle\Entity\Artist;
use MadeSimple\Bundle\MusicLibraryBundle\Form\Type\ArtistType;
use MadeSimple\Bundle\MusicLibraryBundle\Service\Resource\ArtistManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for artists
 * @Route("/artists")
 */
class ArtistController extends Controller
{
    /**
     * @param Request $request
     * @return array|Response
     *
     * @Route(path="/", name="artist.list")
     * @Template()
     */
    public function listAction(Request $request)
    {
        $parameters = $request->query;

        $page = $parameters->get('page', 1);
        $limit = $this->getParameter('music_library.artist.list.limit');
        $order = null;

        if ($parameters->has('order') && $parameters->has('order_type')) {
            $order = [
                $parameters->get('order') => $parameters->get('order_type')
            ];
        }

        $manager = $this->getArtistManager();

        /** @var Artist[] $artists */
        $artists = $manager->getList([], $order, $page, $limit);

        return [
            'artists' => $artists,
            'count' => $this->getArtistManager()->count(),
            'limit' => $limit
        ];
    }

    /**
     * @param Request $request
     * @return Response|array
     *
     * @Route(path="/add", name="artist.add")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $form = $this->get('form.factory')->create(ArtistType::class);
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var Artist $artist */
            $artist = $form->getData();
            try {
                $this->getArtistManager()->add($artist);
                $this->addFlash('success', 'Artist has been successfully added.');
            } catch (ResourceOperationException $e) {
                $this->addFlash('error', 'Artist has not been added!');
            }

            return $this->redirectToRoute('artist.detail', ['artistId' => $artist->getId()]);
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @param Request $request
     * @param int $artistId
     * @return Response|array
     *
     * @Route(path="/edit/{artistId}", name="artist.edit")
     * @Template()
     */
    public function editAction(Request $request, $artistId)
    {
        try {
            /** @var Artist $artist */
            $artist = $this->getArtistManager()->get($artistId);
        } catch (ResourceNotFoundException $e) {
            $this->addFlash('error', "Artist with Id {$artistId} has not been found!");

            return $this->redirectToRoute('artist.list');
        }

        $form = $this->get('form.factory')->create(ArtistType::class, $artist);
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var Artist $artist */
            $artist = $form->getData();
            $this->getArtistManager()->update($artist);
            try {
                $this->addFlash('success', 'Artist has been successfully updated.');
            } catch (ResourceOperationException $e) {
                $this->addFlash('error', 'Artist has not been updated!');
            }

            return $this->redirectToRoute('artist.detail', ['artistId' => $artist->getId()]);
        }

        return [
            'artist' => $artist,
            'form' => $form->createView()
        ];
    }

    /**
     * @param int $artistId
     * @return Response|array
     *
     * @Route(path="/detail/{artistId}", name="artist.detail")
     * @Template()
     */
    public function detailAction($artistId)
    {
        try {
            /** @var Artist $artist */
            $artist = $this->getArtistManager()->get($artistId);
        } catch (ResourceNotFoundException $e) {
            $this->addFlash('error', "Artist with Id {$artistId} has not been found!");

            return $this->redirectToRoute('artist.list');
        }

        return [
            'artist' => $artist
        ];
    }

    /**
     * @param int $artistId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route(path="/delete/{artistId}", name="artist.delete")
     */
    public function deleteAction($artistId)
    {
        try {
            /** @var Artist $artist */
            $artist = $this->getArtistManager()->get($artistId);
            $this->getArtistManager()->remove($artist);
            $this->addFlash('success', 'Artist has been successfully deleted.');

            return $this->redirectToRoute('artist.list');
        } catch (ResourceNotFoundException $e) {
            $this->addFlash('error', "Artist with Id {$artistId} has not been found!");
        } catch (ResourceOperationException $e) {
            $this->addFlash('error', 'Artist has not been deleted!');
        }

        return $this->redirectToRoute('artist.detail', ['artistId' => $artistId]);
    }

    /**
     * Returns artist manager service
     * @return ArtistManager
     */
    private function getArtistManager()
    {
        return $this->get('made_simple_music_library.artist.resource_manager');
    }
}

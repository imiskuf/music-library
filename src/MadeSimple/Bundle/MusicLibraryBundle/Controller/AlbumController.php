<?php

namespace MadeSimple\Bundle\MusicLibraryBundle\Controller;

use AppBundle\Model\Exception\ResourceNotFoundException;
use AppBundle\Model\Exception\ResourceOperationException;
use MadeSimple\Bundle\MusicLibraryBundle\Entity\Album;
use MadeSimple\Bundle\MusicLibraryBundle\Form\Type\AlbumType;
use MadeSimple\Bundle\MusicLibraryBundle\Service\Resource\AlbumManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for albums
 * @Route("/albums")
 */
class AlbumController extends Controller
{
    /**
     * @param Request $request
     * @return array|Response
     *
     * @Route(path="/", name="album.list")
     * @Template()
     */
    public function listAction(Request $request)
    {
        $parameters = $request->query;

        $page = $parameters->get('page', 1);
        $limit = $this->getParameter('music_library.album.list.limit');
        $order = null;

        if ($parameters->has('order') && $parameters->has('order_type')) {
            $order = [
                $parameters->get('order') => $parameters->get('order_type')
            ];
        }

        $manager = $this->getAlbumManager();

        /** @var Album[] $albums */
        $album = $manager->getList([], $order, $page, $limit);

        return [
            'albums' => $album,
            'count' => $this->getAlbumManager()->count(),
            'limit' => $limit
        ];
    }

    /**
     * @param Request $request
     * @return Response|array
     *
     * @Route(path="/add", name="album.add")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $form = $this->get('form.factory')->create(AlbumType::class);
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var Album $album */
            $album = $form->getData();
            try {
                $this->getAlbumManager()->add($album);
                $this->addFlash('success', 'Album has been successfully added.');
            } catch (ResourceOperationException $e) {
                $this->addFlash('error', 'Album has not been added!');
            }

            return $this->redirectToRoute('album.detail', ['albumId' => $album->getId()]);
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @param Request $request
     * @param int $albumId
     * @return Response|array
     *
     * @Route(path="/edit/{albumId}", name="album.edit")
     * @Template()
     */
    public function editAction(Request $request, $albumId)
    {
        try {
            /** @var Album $album */
            $album = $this->getAlbumManager()->get($albumId);
        } catch (ResourceNotFoundException $e) {
            $this->addFlash('error', "Album with Id {$albumId} has not been found!");

            return $this->redirectToRoute('album.list');
        }

        $form = $this->get('form.factory')->create(AlbumType::class, $album);
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var Album $album */
            $album = $form->getData();
            $this->getAlbumManager()->update($album);
            try {
                $this->addFlash('success', 'Album has been successfully updated.');
            } catch (ResourceOperationException $e) {
                $this->addFlash('error', 'Album has not been updated!');
            }

            return $this->redirectToRoute('album.detail', ['albumId' => $album->getId()]);
        }

        return [
            'album' => $album,
            'form' => $form->createView()
        ];
    }

    /**
     * @param int $albumId
     * @return Response|array
     *
     * @Route(path="/detail/{albumId}", name="album.detail")
     * @Template()
     */
    public function detailAction($albumId)
    {
        try {
            /** @var Album $album */
            $album = $this->getAlbumManager()->get($albumId);
        } catch (ResourceNotFoundException $e) {
            $this->addFlash('error', "Album with Id {$albumId} has not been found!");

            return $this->redirectToRoute('album.list');
        }

        return [
            'album' => $album
        ];
    }

    /**
     * @param int $albumId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route(path="/delete/{albumId}", name="album.delete")
     */
    public function deleteAction($albumId)
    {
        try {
            /** @var Album $album */
            $album = $this->getAlbumManager()->get($albumId);
            $this->getAlbumManager()->remove($album);
            $this->addFlash('success', 'Album has been successfully deleted.');

            return $this->redirectToRoute('album.list');
        } catch (ResourceNotFoundException $e) {
            $this->addFlash('error', "Album with Id {$albumId} has not been found!");
        } catch (ResourceOperationException $e) {
            $this->addFlash('error', 'Album has not been deleted!');
        }

        return $this->redirectToRoute('album.detail', ['albumId' => $albumId]);
    }

    /**
     * Returns album manager service
     * @return AlbumManager
     */
    private function getAlbumManager()
    {
        return $this->get('made_simple_music_library.album.resource_manager');
    }
}

<?php

namespace MadeSimple\Bundle\MusicLibraryBundle\Entity;

use AppBundle\Entity\Traits\LifecycledTimestampsTrait;
use AppBundle\Model\ResourceInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Artist entity
 * @Vich\Uploadable()
 * @ORM\Entity()
 * @ORM\Table(name="music_library_artist")
 * @ORM\HasLifecycleCallbacks()
 */
class Artist implements ResourceInterface
{
    use LifecycledTimestampsTrait;

    /**
     * Identifier
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Name of artist
     * @var string
     *
     * @ORM\Column(type="string", length=64)
     * @Assert\NotBlank(message="Artist's name cannot be blank!")
     */
    private $name;

    /**
     * Country
     * @var string
     *
     * @ORM\Column(type="string", length=8)
     * @Assert\NotBlank(message="Country cannot be blank!")
     */
    private $country;

    /**
     * Genre
     * @var string
     *
     * @ORM\Column(type="string", length=32)
     * @Assert\NotBlank(message="Genre cannot be blank!")
     */
    private $genre;

    /**
     * Relative path of image
     * @var string
     *
     * @ORM\Column(type="string", length=256, nullable=true)
     */
    private $image;

    /**
     * Url to more infomration
     * @var string
     *
     * @ORM\Column(type="string", length=256, nullable=true)
     * @Assert\Url()
     */
    private $url;

    /**
     * File object of image
     * @var File
     *
     * @Vich\UploadableField(mapping="music_library_artist_image", fileNameProperty="image")
     * @Assert\Image()
     */
    private $imageFile;

    /**
     * Albums
     * @var ArrayCollection|Album[]
     *
     * @ORM\OneToMany(targetEntity="MadeSimple\Bundle\MusicLibraryBundle\Entity\Album", mappedBy="artist")
     */
    private $albums;

    /**
     * Getter for id
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Setter for id
     * @param mixed $id
     * @return Artist
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Getter for name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Setter for name
     * @param string $name
     * @return Artist
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Getter for country
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Setter for country
     * @param string $country
     * @return Artist
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Getter for genre
     * @return mixed
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * Setter for genre
     * @param mixed $genre
     * @return Artist
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * Getter for image
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Setter for image
     * @param string $image
     * @return Artist
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Getter for url
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Setter for url
     * @param string $url
     * @return Artist
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Setter for image file
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Artist
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        $this->setModifiedAt(new \DateTime());

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Getter for albums
     * @return ArrayCollection|Album[]
     */
    public function getAlbums()
    {
        return $this->albums;
    }

    /**
     * Setter for albums
     * @param ArrayCollection|Album[] $albums
     */
    public function setAlbums($albums)
    {
        $this->albums = $albums;
    }
}

<?php
/**
 * @author    imiskuf
 * @copyright Pixel federation
 * @license:  Internal use only
 * Date: 17.3.2017
 * Time: 0:25
 */

namespace MadeSimple\Bundle\MusicLibraryBundle\Entity;

use AppBundle\Entity\Traits\LifecycledTimestampsTrait;
use AppBundle\Model\ResourceInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class Album
 * @package MadeSimple\Bundle\MusicLibraryBundle\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="music_library_album")
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable()
 */
class Album implements ResourceInterface
{
    use LifecycledTimestampsTrait;

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64)
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var Artist
     *
     * @ORM\ManyToOne(targetEntity="MadeSimple\Bundle\MusicLibraryBundle\Entity\Artist", inversedBy="albums")
     * @ORM\JoinColumn(name="artist_id", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\NotNull()
     */
    private $artist;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @Assert\Range(min="1980", max="2020")
     * @Assert\NotBlank()
     */
    private $year;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @Assert\Range(min="1", max="100")
     * @Assert\NotBlank()
     */
    private $songCount;

    /**
     * Relative path of image
     * @var string
     *
     * @ORM\Column(type="string", length=256, nullable=true)
     */
    private $image;

    /**
     * File object of image
     * @var File
     *
     * @Vich\UploadableField(mapping="music_library_album_image", fileNameProperty="image")
     * @Assert\Image()
     */
    private $imageFile;

    /**
     * Getter for id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Setter for id
     * @param int $id
     * @return Album
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Getter for title
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Setter for title
     * @param string $title
     * @return Album
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Getter for artist
     * @return Artist
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * Setter for artist
     * @param Artist $artist
     * @return Album
     */
    public function setArtist($artist)
    {
        $this->artist = $artist;

        return $this;
    }

    /**
     * Getter for year
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Setter for year
     * @param int $year
     * @return Album
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Getter for songCount
     * @return int
     */
    public function getSongCount()
    {
        return $this->songCount;
    }

    /**
     * Setter for songCount
     * @param int $songCount
     * @return Album
     */
    public function setSongCount($songCount)
    {
        $this->songCount = $songCount;

        return $this;
    }

    /**
     * Getter for image
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Setter for image
     * @param string $image
     * @return Album
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Setter for image file
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Album
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        $this->setModifiedAt(new \DateTime());

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }


}

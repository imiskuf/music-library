<?php
/**
 * @author    imiskuf
 * @copyright Pixel federation
 * @license:  Internal use only
 * Date: 12.3.2017
 * Time: 22:11
 */

namespace AppBundle\Entity\Traits;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class LifecycledTimestampsTrait
 * @package AppBundle\Entity\Traits
 */
trait LifecycledTimestampsTrait
{
    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", name="created_at", nullable=false)
     */
    private $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", name="modified_at", nullable=true)
     */
    private $modifiedAt;

    /**
     * Getter for createdAt
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Setter for createdAt
     * @param DateTime $createdAt
     * @return LifecycledTimestampsTrait
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Getter for modifiedAt
     * @return DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Setter for modifiedAt
     * @param DateTime $modifiedAt
     * @return LifecycledTimestampsTrait
     */
    public function setModifiedAt(DateTime $modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Automatic date and time of creation
     * @ORM\PrePersist()
     */
    public function onPrePersist()
    {
        $this->setCreatedAt(new DateTime());
    }

    /**
     * Automatic date ant time of modification
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->setModifiedAt(new DateTime());
    }
}

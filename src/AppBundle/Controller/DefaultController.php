<?php
/**
 * @author    imiskuf
 * @copyright Pixel federation
 * @license:  Internal use only
 * Date: 12.2.2017
 * Time: 10:53
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * Homepage
     * @return RedirectResponse
     *
     * @Route("/", name="app")
     */
    public function homepageAction()
    {
        return $this->redirectToRoute('artist.list');
    }
}

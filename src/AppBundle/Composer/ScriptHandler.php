<?php
/**
 * @author    imiskuf
 * @license:  Internal use only
 */

namespace AppBundle\Composer;

use Composer\Script\Event;
use FilesystemIterator;
use Sensio\Bundle\DistributionBundle\Composer\ScriptHandler as SymfonyScriptHandler;

/**
 * Custom script handler for application
 * @package AppBundle\Composer
 */
class ScriptHandler extends SymfonyScriptHandler
{
    /**
     * Executes database migration only if at least one migration script exists.
     * @param Event $event
     */
    public static function handleMigrations(Event $event)
    {
        $options = static::getOptions($event);
        $migrationsDir = $options['symfony-app-dir'] . '/migrations';
        $consoleDir = $options['symfony-bin-dir'];

        if (!file_exists($migrationsDir)) {
            return;
        }

        $iterator = new FilesystemIterator($migrationsDir, FilesystemIterator::SKIP_DOTS);

        echo "\nChecking migrations\n\n";

        if (0 === iterator_count($iterator)) {
            echo "\nNo migrations detected. Skipping.\n\n";

            return;
        }

        static::executeCommand($event, $consoleDir, 'doctrine:migrations:migrate --no-interaction');
    }
}

<?php
/**
 * @author    imiskuf
 * @copyright Pixel federation
 * @license:  Internal use only
 * Date: 16.3.2017
 * Time: 12:28
 */

namespace AppBundle\Service\Common;

use AppBundle\Model\CountableInterface;
use AppBundle\Model\Exception\ResourceOperationException;
use AppBundle\Model\ResourceInterface;
use AppBundle\Model\Exception\ResourceNotFoundException;

/**
 * Interface ResourceManagerInterface
 * @package AppBundle\Service\Common
 */
interface ResourceManagerInterface extends CountableInterface
{
    /**
     * Adds resoruce
     * @param ResourceInterface $resource
     * @throws ResourceOperationException
     */
    public function add(ResourceInterface $resource);

    /**
     * Removes resource
     * @param ResourceInterface $resource
     * throws ResourceOperationException
     */
    public function remove(ResourceInterface $resource);

    /**
     * Updates resoruce
     * @param ResourceInterface $resource
     * @throws ResourceOperationException
     */
    public function update(ResourceInterface $resource);

    /**
     * Gets the resource by id or throws not found exception
     * @param int $resourceId
     * @return ResourceInterface
     * @throws ResourceNotFoundException
     */
    public function get($resourceId);

    /**
     * Returns list of resources
     * @param array $conditions
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     */
    public function getList(array $conditions = [], array $orderBy = null, $limit = null, $offset = null);
}

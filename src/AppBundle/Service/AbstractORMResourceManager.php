<?php
/**
 * @author    imiskuf
 * @copyright Pixel federation
 * @license:  Internal use only
 * Date: 16.3.2017
 * Time: 12:27
 */

namespace AppBundle\Service;

use AppBundle\Model\Exception\ResourceNotFoundException;
use AppBundle\Model\Exception\ResourceOperationException;
use AppBundle\Model\ResourceInterface;
use AppBundle\Service\Common\ResourceManagerInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Doctrine\ORM\TransactionRequiredException;

/**
 * Class AbstractDoctrineResourceManager
 * @package AppBundle\Service
 */
abstract class AbstractORMResourceManager implements ResourceManagerInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * Returns resource class
     * @return string
     */
    abstract protected function getResourceClass();

    /**
     * AbstractDoctrineResourceManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Adds resoruce
     * @param ResourceInterface $resource
     * @throws ResourceOperationException
     */
    public function add(ResourceInterface $resource)
    {
        try {
            $this->em->persist($resource);
            $this->em->flush($resource);
        } catch (ORMInvalidArgumentException $e) {
            throw new ResourceOperationException('Resource adding failed!', 0, $e);
        } catch (ORMException $e) {
            throw new ResourceOperationException('Resource adding failed!', 0, $e);
        }
    }

    /**
     * Removes resource
     * @param ResourceInterface $resource
     * @throws ResourceOperationException
     */
    public function remove(ResourceInterface $resource)
    {
        try {
            $this->em->remove($resource);
            $this->em->flush($resource);
        } catch (ORMInvalidArgumentException $e) {
            throw new ResourceOperationException('Resource removing failed!', 0, $e);
        } catch (ORMException $e) {
            throw new ResourceOperationException('Resource removing failed!', 0, $e);
        }
    }

    /**
     * Updates resoruce
     * @param ResourceInterface $resource
     * @throws ResourceOperationException
     */
    public function update(ResourceInterface $resource)
    {
        try {
            $this->em->flush($resource);
        } catch (ORMException $e) {
            throw new ResourceOperationException('Resource updating failed!', 0, $e);
        }
    }

    /**
     * Gets the resource by id or throws not found exception
     * @param int $resourceId
     * @return ResourceInterface
     * @throws ResourceNotFoundException
     * @throws ORMInvalidArgumentException
     * @throws OptimisticLockException
     * @throws ORMException
     * @throws TransactionRequiredException
     */
    public function get($resourceId)
    {
        /** @var ResourceInterface $resource */
        $resource = $this->em->find($this->getResourceClass(), $resourceId);
        if (null === $resource) {
            throw new ResourceNotFoundException($resourceId);
        }

        return $resource;
    }

    /**
     * Returns list of resources
     * @param array $conditions
     * @param array|null $orderBy
     * @param int|null $page
     * @param int|null $limit
     * @return ResourceInterface[]
     */
    public function getList(array $conditions = [], array $orderBy = null, $page = null, $limit = null)
    {
        $repository = $this->em->getRepository($this->getResourceClass());

        return $repository->findBy([], $orderBy, $limit, $limit * ($page - 1));
    }

    /**
     * Returns count of objects
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function count()
    {
        $dql = 'SELECT COUNT(p) FROM  ' . $this->getResourceClass() . ' p';

        return $this->em->createQuery($dql)->getSingleScalarResult();
    }


}

<?php
/**
 * @author    imiskuf
 * @copyright Pixel federation
 * @license:  Internal use only
 * Date: 16.3.2017
 * Time: 23:43
 */

namespace AppBundle\Model;

/**
 * Interface CountableInterface
 * @package AppBundle\Model
 */
interface CountableInterface
{
    /**
     * Returns count of objects
     * @return int
     */
    public function count();
}
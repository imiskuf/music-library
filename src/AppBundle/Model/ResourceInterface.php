<?php

namespace AppBundle\Model;

/**
 * Interface ResourceInterface
 * @package AppBundle\Model
 */
interface ResourceInterface
{
    /**
     * Returns identifier
     * @return int
     */
    public function getId();
}

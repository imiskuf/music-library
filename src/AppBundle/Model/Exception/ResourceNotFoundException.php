<?php

namespace AppBundle\Model\Exception;

use Exception;
use RuntimeException;

/**
 * Class ResourceNotFoundException
 */
class ResourceNotFoundException extends RuntimeException
{
    /**
     * Resource ID
     * @var int
     */
    private $resourceId;

    /**
     * ResourceNotFoundException constructor.
     * @param int $resourceId
     * @param string $message
     * @param int $code
     * @param Exception|null $previousException
     */
    public function __construct($resourceId, $message = '', $code = 0, $previousException = null)
    {
        parent::__construct($message, $code, $previousException);

        $this->resourceId = $resourceId;
    }

    /**
     * Getter for resourceId
     * @return int
     */
    public function getResourceId()
    {
        return $this->resourceId;
    }
}

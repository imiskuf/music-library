<?php
/**
 * @author    imiskuf
 * @copyright Pixel federation
 * @license:  Internal use only
 * Date: 16.3.2017
 * Time: 22:57
 */

namespace AppBundle\Model\Exception;

use RuntimeException;

/**
 * Class ResourceOperationException
 * @package AppBundle\Model\Exception
 */
class ResourceOperationException extends RuntimeException
{

}

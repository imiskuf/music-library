Instrukcie
==========


1. Je potrebne vytvorit prazdnu databazu, napriklad `music`
2. Treba si otvorit konzolu v korenovom priecinku aplikacie
3. V konzole spustit prikaz:

```php composer.phar install```

Tento prikaz nainstaluje aplikaciu. Instalacny proces sa bude pytat na zopar udajov, ako aj na pripojenie k databaze a nazov databazy. Sem treba zadat nazov novo vytvorenej databazy (z prikladu hore je to `music`). Ostatne staci viac menej potvrdit enterom

4. Teraz je potrebne spusit server s aplikaciou a to prikazom:

```php bin/console server:run```

Pri tomto prikaze vypise konzola chybu, ale netreba sa zlaknut, je to len kvoli tomu ze som pripravil 1 environment a to rovno produkcny.

5. Teraz je potrebne navstivit url, ktoru vypise konzola, defaulne by to malo byt:

```http://localhost:8000```